<?php

use app\models\Users;
use execut\widget\TreeView;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Users $model
 */

$this->title = 'Реферальная сеть клиента «'.$model->nameFormat .'»';
?>

<h2>Узкие места БД</h2>
<ul>
    <li>Не проиндексированы поля</li>
    <li>Не настроены ключи</li>
    <li>Не понятно, нужны ли уникальные поля или нет (Например client_uid в таблице «accounts»)</li>
</ul>

<hr>

<!--<h2>Тестовое дерево</h2>-->
<!---->
<?//= TreeView::widget([
//    'data' => Users::buildTreeArray(Users::$items),
//    'template' => TreeView::TEMPLATE_SIMPLE,
//    'size' => TreeView::SIZE_SMALL,
//])?>

<div class="row">
    <div class="col-md-9">
        <h2 style="margin-top: 0">
            <?= $this->title?>
        </h2>
    </div>
    <div class="col-md-3">
        <?= Html::beginForm(['/test'], 'get')?>
        <?= Html::textInput('client_uid', Yii::$app->request->get('client_uid'), [
            'placeholder' => 'Поиск по client_uid',
            'class' => 'form-control'
        ])?>
        <?= Html::endForm()?>
    </div>
</div>

<ul>
    <li><b>Количество прямых рефералов</b>: <?=$model->countDirectReferral?></li>
    <li><b>Количество всех рефералов клиента</b>: <?=$model->countTotalReferral?></li>
    <li><b>Количество уровней реферальной сетки</b>: <?=$model->countLevelsReferral?></li>
</ul>

<?= TreeView::widget([
    'data' => $model->tree,
    'template' => TreeView::TEMPLATE_SIMPLE,
    'size' => TreeView::SIZE_SMALL,
])?>

<hr>

<h2>Суммарный объем volume * coeff_h * coeff_cr по всем уровням реферральной системы за период времени</h2>
<?= $this->render('_filter', ['model' => $model, 'action' => Url::to(['/test/calc-volume-coeff'])]) ?>

<hr>

<h2>Прибыльность (сумма profit) за определенный период времени</h2>
<?= $this->render('_filter', ['model' => $model, 'action' => Url::to(['/test/calc-profit'])]) ?>

<br>

<p>Реализация должна быть в фоновом режиме, однако на это требуется больше времени</p>

