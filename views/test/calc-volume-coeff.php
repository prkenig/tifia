<?php

use app\models\Users;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Users $model
 * @var string|null $datetime_from
 * @var string|null $datetime_to
 */

$this->title = 'Суммарный объем volume * coeff_h * coeff_cr по всем уровням реферральной системы';

$total = 0;
?>

<h1><?= $this->title?></h1>

<br>

<?= $this->render('_filter', ['model' => $model, 'action' => Url::to(['/test/calc-volume-coeff'])]) ?>

<br>

<table class="table">
    <tr>
        <th>Реферал</th>
        <th>SUM(volume * coeff_h * coeff_cr)</th>
    </tr>
    <? foreach ($model->allReferrals as $referal):
        $sumVolumeCoeff = $referal->calcSumVolumeCoeff($datetime_from, $datetime_to);
        $total += $sumVolumeCoeff;
        ?>
        <tr>
            <td>
                <?= $referal->nameFormat ?>
            </td>
            <td>
                <?= $sumVolumeCoeff ?>
            </td>
        </tr>
    <? endforeach;?>
    <tr>
        <td>
            <b>ИТОГО:</b>
        </td>
        <td><?= $total?></td>
    </tr>
</table>
