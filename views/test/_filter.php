<?php

use app\models\Users;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var Users $model
 * @var string $action
 */

?>

<?= Html::beginForm($action, 'get')?>
<?= Html::hiddenInput('client_uid', $model->client_uid) ?>
    <div class="row">
        <div class="col-md-4">
            <?= DateTimePicker::widget([
                'name' => 'datetime_from',
                'value' => Yii::$app->request->get('datetime_from'),
                'options' => [
                   'placeholder' => 'С',
                ]
            ])?>
        </div>
        <div class="col-md-4">
            <?= DateTimePicker::widget([
                'name' => 'datetime_to',
                'value' => Yii::$app->request->get('datetime_to'),
                'options' => [
                    'placeholder' => 'По',
                ]
            ])?>
        </div>
        <div class="col-md-4"><?= Html::submitButton('Посчитать', ['class' => 'btn btn-block btn-primary'])?></div>
    </div>
<?= Html::endForm()?>