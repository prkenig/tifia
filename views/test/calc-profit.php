<?php

use app\models\Users;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Users $model
 * @var string|null $datetime_from
 * @var string|null $datetime_to
 */

$this->title = 'Прибыльность (сумма profit)';

$total = 0;
?>

<h1><?= $this->title?></h1>

<br>

<?= $this->render('_filter', ['model' => $model, 'action' => Url::to(['/test/calc-profit'])]) ?>

<br>

<table class="table">
    <tr>
        <th>Реферал</th>
        <th>SUM(profit)</th>
    </tr>
    <? foreach ($model->allReferrals as $referal):
        $sumProfit = $referal->calcSumProfit($datetime_from, $datetime_to);
        $total += $sumProfit;
        ?>
        <tr>
            <td>
                <?= $referal->nameFormat ?>
            </td>
            <td>
                <?= $sumProfit ?>
            </td>
        </tr>
    <? endforeach;?>
    <tr>
        <td>
            <b>ИТОГО:</b>
        </td>
        <td><?= $total?></td>
    </tr>
</table>