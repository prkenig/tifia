<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "accounts".
 *
 * @property int $id
 * @property int|null $client_uid
 * @property int|null $login
 * @property-read Users $user
 * @property-read Trades[] $trades
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_uid', 'login'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_uid' => 'Client Uid',
            'login' => 'Login',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Users::class, ['client_uid' => 'client_uid']);
    }

    public function getTrades()
    {
        return $this->hasMany(Trades::class, ['login' => 'login']);
    }
}
