<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property int|null $client_uid
 * @property string|null $email
 * @property string|null $gender
 * @property string|null $fullname
 * @property string|null $country
 * @property string|null $region
 * @property string|null $city
 * @property string|null $address
 * @property int|null $partner_id
 * @property string|null $reg_date
 * @property int|null $status
 *
 * @property-read Users $partner
 * @property-read Accounts $account
 * @property-read Trades[] $trades
 * @property-read array $tree
 * @property-read string $nameFormat
 * @property-read integer $countDirectReferral
 * @property-read integer $countTotalReferral
 * @property-read integer $countLevelsReferral
 * @property-read Users[] $allReferrals
 */
class Users extends \yii\db\ActiveRecord
{
    private $_tree;
    private $_allReferrals;

    public static $items = [
        ['client_uid' => 123, 'partner_id' => 0],
        ['client_uid' => 456, 'partner_id' => 0],
        ['client_uid' => 312, 'partner_id' => 0],
        ['client_uid' => 471, 'partner_id' => 123],
        ['client_uid' => 965, 'partner_id' => 123],
        ['client_uid' => 892, 'partner_id' => 965],
        ['client_uid' => 111, 'partner_id' => 456],
        ['client_uid' => 222, 'partner_id' => 111],
        ['client_uid' => 333, 'partner_id' => 111],
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_uid', 'partner_id', 'status'], 'integer'],
            [['reg_date'], 'safe'],
            [['email'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 5],
            [['fullname'], 'string', 'max' => 150],
            [['country'], 'string', 'max' => 2],
            [['region', 'city'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_uid' => 'Client Uid',
            'email' => 'Email',
            'gender' => 'Gender',
            'fullname' => 'Fullname',
            'country' => 'Country',
            'region' => 'Region',
            'city' => 'City',
            'address' => 'Address',
            'partner_id' => 'Partner ID',
            'reg_date' => 'Reg Date',
            'status' => 'Status',
        ];
    }

    public function getPartner()
    {
        return $this->hasOne(static::class, ['partner_id' => 'client_uid']);
    }

    public function getAccount()
    {
        return $this->hasOne(Accounts::class, ['client_uid' => 'client_uid']);
    }

    public function getTrades()
    {
        return $this->hasMany(Trades::class, ['login' => 'login'])
            ->via('account');
    }

    public function getTree()
    {
        if(!empty($this->_tree)) return $this->_tree;

        $models = Users::searchModelsByPartnerId($this->client_uid);
        $data = Users::buildTree($models, $this->client_uid);

        return $this->_tree = $data;
    }

    public function getNameFormat()
    {
        return $this->email . ' (' . $this->client_uid.')';
    }

    public function getCountDirectReferral()
    {
        return count($this->tree);
    }

    public function getCountTotalReferral()
    {
        return count($this->allReferrals);
    }

    public function getCountLevelsReferral()
    {
        return self::countLevels($this->tree);
    }

    public function getAllReferrals()
    {
        if(!empty($this->_allReferrals)) return $this->_allReferrals;

        return $this->_allReferrals = self::allReferrals($this->tree);
    }

    public function calcSumVolumeCoeff($from = null, $to = null)
    {
        return Yii::$app->cache->getOrSet(['sum_volume_coeff', $this->client_uid, $from, $to], function () use ($from, $to) {
            return $this->takeSum('volume * coeff_h * coeff_cr', $from, $to);
        });
    }

    public function calcSumProfit($from = null, $to = null)
    {
        return Yii::$app->cache->getOrSet(['sum_profit', $this->client_uid, $from, $to], function () use ($from, $to) {
            return $this->takeSum('profit', $from, $to);
        });
    }

    /**
     * @param Users[] $models
     * @param int $partner_id
     * @return array
     */
    protected static function buildTree(array &$models, $partner_id = 0)
    {
        $result = [];

        foreach ($models as $model) {
            $data = [];
            if ($model->partner_id == $partner_id) {
                $childrenModels = self::searchModelsByPartnerId($model->client_uid);
                $children = self::buildTree($childrenModels, $model->client_uid);
                if (!empty($children)) $data['nodes'] = $children;
                $data['text'] = $model->nameFormat;
                $data['model'] = $model;
                $result[] = $data;
            }
        }

        return $result;
    }

    public static function buildTreeArray(array &$arr, $partner_id = 0)
    {
        $result = [];

        foreach ($arr as $model) {
            $data = [];
            if ($model['partner_id'] == $partner_id) {
                $children = self::buildTreeArray($arr, $model['client_uid']);
                if (!empty($children)) $data['nodes'] = $children;
                $data['text'] = $model['client_uid'];
                $result[] = $data;
            }
        }

        return $result;
    }

    protected static function searchModelsByPartnerId($client_uid)
    {
        // КЕШ!!!
        return Yii::$app->cache->getOrSet(['tifia_users', $client_uid], function () use ($client_uid) {
            return self::find()->andWhere(['partner_id' => $client_uid])->all();
        });
    }

    protected static function allReferrals(array $arr)
    {
        $models = [];

        foreach ($arr as $i => $item) {
            $models[] = $item['model'];

            if(isset($item['nodes'])){
                $models = ArrayHelper::merge($models, self::allReferrals($item['nodes']));
            }
        }

        return $models;
    }

    protected static function countLevels(array $arr)
    {
        $max_lvl = 1;

        foreach ($arr as $item) {
            if (isset($item['nodes'])) {
                $lvl = self::countLevels($item['nodes']) + 1;
                if ($lvl > $max_lvl) $max_lvl = $lvl;
            }
        }

        return $max_lvl;
    }

    protected function takeSum($field, $from = null, $to = null)
    {
        $query = $this->getTrades()
            ->select(['sum' => 'SUM('.$field.')'])
            ->groupBy('login')
            ->asArray();

        // период времени определяет интервал поля close_time таблицы Trades
        if(!empty($from) && !empty($to)){
            $query->andWhere(['between', 'close_time', $from, $to]);
        } elseif(!empty($from)){
            $query->andWhere(['>=', 'close_time', $from]);
        } elseif(!empty($to)){
            $query->andWhere(['<=', 'close_time', $to]);
        }

        $result = $query->one();

        return ArrayHelper::getValue($result, 'sum', 0);
    }
}
