<?php


namespace app\controllers;


use app\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class TestController extends Controller
{
    const DEFAULT_CLIENT_UID = 67153006; // 82824897

    public function actionIndex($client_uid = null)
    {
        $model = $this->findModel($client_uid);

        return $this->render('index', ['model' => $model]);
    }

    public function actionCalcVolumeCoeff($client_uid, $datetime_from = null, $datetime_to = null)
    {
        $model = $this->findModel($client_uid);

        return $this->render('calc-volume-coeff', ['model' => $model, 'datetime_from' => $datetime_from, 'datetime_to' => $datetime_to]);
    }


    public function actionCalcProfit($client_uid, $datetime_from = null, $datetime_to = null)
    {
        $model = $this->findModel($client_uid);

        return $this->render('calc-profit', ['model' => $model, 'datetime_from' => $datetime_from, 'datetime_to' => $datetime_to]);
    }

    protected function findModel($client_uid)
    {
        $client_uid = empty($client_uid) ? self::DEFAULT_CLIENT_UID : $client_uid;

        $model = Users::findOne(['client_uid' => $client_uid]);

        if(empty($model)) throw new NotFoundHttpException('Клиент не найден');

        return $model;
    }
}
